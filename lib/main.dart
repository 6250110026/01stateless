import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_photo),
          title: Text('My Mo By NM+'),
          actions: [IconButton(onPressed: (){}, icon: Icon(Icons.add_to_drive))],
        ),
        body: Center(
          child:Text('Nuchanath Mohlea.',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 35
          ),),
        ),
      ),
    );
  }
}
